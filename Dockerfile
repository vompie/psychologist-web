FROM python:3.9-alpine

# RUN mkdir -p /usr/src/app/
# COPY . /usr/src/app/

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

WORKDIR /app
COPY requirements.txt /app/


# RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/main" > /etc/apk/repositories
# RUN echo "http://dl-cdn.alpinelinux.org/alpine/latest-stable/community" >> /etc/apk/repositories

#RUN \
# apk add --no-cache --update-cache gfortran python3-dev py-pip wget freetype-dev libpng-dev openblas-dev && \
# apk add --no-cache postgresql-libs && \
# apk add --no-cache --virtual .build-deps gcc musl-dev postgresql-dev && \
# apk add --no-cach build-base && \
# pip install -r requirements.txt --no-cache-dir && \
# apk --purge del .build-deps

# RUN apt-get update
RUN apk add --no-cache --virtual .build-deps \
    ca-certificates gcc postgresql-dev linux-headers musl-dev \
    libffi-dev jpeg-dev zlib-dev
RUN apk add nano
#ffmpeg libsm6 libxext6 libdc1394-dev -y

RUN pip install -r requirements.txt

COPY . /app/

ADD entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

ENV TZ Europe/Moscow

# ADD docker-entrypoint.sh /docker-entrypoint.sh
# RUN chmod a+x /docker-entrypoint.sh
# ENTRYPOINT ["/docker-entrypoint.sh"]
# command: python manage.py runserver 0.0.0.0:8000
