from django.db import models

from django.db.models.signals import pre_delete, post_delete
from django.dispatch.dispatcher import receiver


class TimeStampMixin(models.Model):
    on_create = models.DateTimeField("Время создания", auto_now_add=True, editable=False, null=False)
    on_change = models.DateTimeField("Время изменения", auto_now=True, null=False)

    class Meta:
        abstract = True


# Article table -> TITLE, ANNOTATION, LOGO (ImageField)
class Article(TimeStampMixin):
    title = models.CharField('Название', max_length=500, null=False, unique=True)
    annotation = models.TextField("Аннотация", max_length=500, null=False)
    logo = models.ImageField("Лого", upload_to='article')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Лекция"
        verbose_name_plural = "Лекции"


# Block Text table -> CONTENT (Text)
class BlockText(TimeStampMixin):
    content = models.TextField("Контент")

    def __str__(self):
        return f'BlockText {self.pk}'

    class Meta:
        verbose_name = "Блок текста"
        verbose_name_plural = "Блоки текстов"


# Block Image table -> CONTENT (ImageField)
class BlockImage(TimeStampMixin):
    content = models.ImageField("Контент", upload_to='image')

    def __str__(self):
        return f'BlockImage {self.pk}'

    class Meta:
        verbose_name = "Блок изображения"
        verbose_name_plural = "Блоки изображений"


# Block Audio table -> CONTENT (FileFiled)
class BlockAudio(TimeStampMixin):
    content = models.FileField("Контент", upload_to='audio')

    def __str__(self):
        return f'BlockAudio {self.pk}'

    class Meta:
        verbose_name = "Блок аудио"
        verbose_name_plural = "Блоки аудио"


# Block Video table -> CONTENT (FileField)
class BlockVideo(TimeStampMixin):
    content = models.FileField("Контент", upload_to='video')

    def __str__(self):
        return f'BlockVideo {self.pk}'

    class Meta:
        verbose_name = "Блок видео"
        verbose_name_plural = "Блоки видео"


# Blocks table -> TYPE (0-4 int), ARTICLE_obj, POSITION_block, BLOCKS_obj * 4
class Block(TimeStampMixin):
    class BlockType(models.IntegerChoices):
        text = 0, 'Текст'
        image = 1, 'Изображение'
        audio = 2, 'Аудио'
        video = 3, 'Видео'

    type = models.PositiveSmallIntegerField('Тип блока', choices=BlockType.choices, null=False)
    article = models.ForeignKey(Article, on_delete=models.CASCADE, null=False, related_name='blocks')
    position = models.PositiveSmallIntegerField('Очередность', null=False)
    block_text = models.ForeignKey(BlockText, on_delete=models.CASCADE, related_name='blocks', null=True, blank=True)
    block_image = models.ForeignKey(BlockImage, on_delete=models.CASCADE, related_name='blocks', null=True, blank=True)
    block_audio = models.ForeignKey(BlockAudio, on_delete=models.CASCADE, related_name='blocks', null=True, blank=True)
    block_video = models.ForeignKey(BlockVideo, on_delete=models.CASCADE, related_name='blocks', null=True, blank=True)

    def __str__(self):
        return f'BlockType {self.type}'

    class Meta:
        verbose_name = "Блок"
        verbose_name_plural = "Блоки"


# receiver for delete article -> try to delete logo of article record
@receiver(post_delete, sender=Article)
def article_model_delete(sender, instance, **kwargs):
    try:
        if instance.logo.name:
            instance.logo.delete(False)
    except:
        pass


# receiver for delete block -> try to delete all blocks record for block record
@receiver(post_delete, sender=Block)
def block_model_delete(sender, instance, **kwargs):
    try:
        if instance.block_text:
            instance.block_text.delete(False)
    except:
        pass
    try:
        if instance.block_image:
            instance.block_image.delete(False)
    except:
        pass
    try:
        if instance.block_audio:
            instance.block_audio.delete(False)
    except:
        pass
    try:
        if instance.block_video:
            instance.block_video.delete(False)
    except:
        pass


# receiver for delete image file for block image record
@receiver(post_delete, sender=BlockImage)
def image_model_delete(sender, instance, **kwargs):
    try:
        if instance.content.name:
            instance.content.delete(False)
    except:
        pass


# receiver for delete audio file for block audio record
@receiver(post_delete, sender=BlockAudio)
def audio_model_delete(sender, instance, **kwargs):
    try:
        if instance.content.name:
            instance.content.delete(False)
    except:
        pass


# receiver for delete video file for block video record
@receiver(post_delete, sender=BlockVideo)
def video_model_delete(sender, instance, **kwargs):
    try:
        if instance.content.name:
            instance.content.delete(False)
    except:
        pass
