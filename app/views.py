from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError
from django.views import View
from . import models
import json
from django.db import transaction
from datetime import datetime, timedelta, timezone
import jwt
from .apps import AppConfig


month_list = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня',
              'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']


# INDEX
class IndexView(View):
    def get(self, request):
        # render index page
        last_article = models.Article.objects.order_by('-on_change')[:6]
        for i in last_article:
            date_list = str(i.on_change).split(" ")[0].split("-")
            i.on_change = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
        # return page, name of page, 6 last articles, and list of articles for contact form
        return render(request, "index.html", {"page": "index", 'last_article': last_article, 'articles_list': get_articles_title()})

    def post(self, request):
        # handle contact form request
        # getting FIO, message, email, phone
        # if contact target is get article -> get article title and id will be 'int'
        # if contact target is get session (id == 'contact_article') -> do nothing
        data = request.POST
        fio = "🧍 {}".format(data['contact_name'])   # fio
        article = "📝 {}".format(data['title'])     # title
        msg = data['contact_message']   # message
        email = "📧 {}".format(data['contact_email'])   # email
        phone = "📞 {}".format(data['contact_phone'])  # phone
        id = data['contact_article']    # id article or just 'contact-to'
        session = False
        if id == "contact-to":  # target is get session with psychologist
            session = True
        # txt will contain:
        #   FIO
        #   \n
        #   title of article or session request with psychologist
        #   message for psychologist
        #   email
        #   phone
        txt = "{0}\n\n{1}\n\n{2}\n{3}\n{4}".format(fio, article, msg, email, phone)
        # send txt massage in telegram chat
        AppConfig.tlg.send_form(txt, id, article, session)
        return HttpResponse()


# Add blocks
def add_blocks(article, text_blocks, image_blocks, audio_blocks, video_blocks):
    # getting only obj.link and create blocks
    if text_blocks:
        res = list(map(lambda x: x['obj'], text_blocks))
        models.BlockText.objects.bulk_create(res)
    if image_blocks:
        res = list(map(lambda x: x['obj'], image_blocks))
        models.BlockImage.objects.bulk_create(res)
    if audio_blocks:
        res = list(map(lambda x: x['obj'], audio_blocks))
        models.BlockAudio.objects.bulk_create(res)
    if video_blocks:
        res = list(map(lambda x: x['obj'], video_blocks))
        models.BlockVideo.objects.bulk_create(res)
    blocks = []
    # create info about block in blocks.model with type, article.id, position, block.link
    for i in text_blocks:
        blocks.append(
            models.Block(
                type=models.Block.BlockType.text._value_,
                article=article,
                position=i['pos'],
                block_text=i['obj']
            )
        )
    for i in image_blocks:
        blocks.append(
            models.Block(
                type=models.Block.BlockType.image._value_,
                article=article,
                position=i['pos'],
                block_image=i['obj']
            )
        )
    for i in audio_blocks:
        blocks.append(
            models.Block(
                type=models.Block.BlockType.audio._value_,
                article=article,
                position=i['pos'],
                block_audio=i['obj']
            )
        )
    for i in video_blocks:
        blocks.append(
            models.Block(
                type=models.Block.BlockType.video._value_,
                article=article,
                position=i['pos'],
                block_video=i['obj']
            )
        )
    models.Block.objects.bulk_create(blocks)


# Get last articles
def get_articles_title():
    # get all articles id and title for contact form and search
    articles_list = models.Article.objects.order_by('-on_change').values('id', 'title') # [:50]
    return articles_list


# CREATE
class CreateView(LoginRequiredMixin, View):
    def get(self, request):
        # render page for create article
        today = datetime.today().strftime("%d.%m.%Y")
        date_list = today.split('.')
        today = date_list[0] + ' ' + month_list[int(date_list[1]) - 1] + ', ' + date_list[2] + ' г.'
        # return page, current date and articles list for contact form
        return render(request, "create.html", {'today': today, 'articles_list': get_articles_title()})

    def post(self, request):
        # save new article in base
        with transaction.atomic():
            # getting files, date and order list for blocks
            files = request.FILES
            data = request.POST
            order = json.loads(data['order'])
            # try to save new article with title, annotation and image (logo)
            try:
                new_article = models.Article(title=data['inp_title_article'], annotation=data['inp_title_annotation'], logo=files['inp_title_img'])
                new_article.save()
            except IntegrityError as e:
                return HttpResponse(status=500, content="IntegrityError")

            text_blocks = []
            image_blocks = []
            audio_blocks = []
            video_blocks = []

            # append new blocks in array with name, obj.link and position
            for key in order:
                if order[key]['type'] == "html":
                    text_blocks.append({"name": key, "obj": models.BlockText(content=data[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "image":
                    image_blocks.append({"name": key, "obj": models.BlockImage(content=files[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "audio":
                    audio_blocks.append({"name": key, "obj": models.BlockAudio(content=files[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "video":
                    video_blocks.append({"name": key, "obj": models.BlockVideo(content=files[key]), 'pos': order[key]['pos']})
            # bulk_create for blocks
            add_blocks(new_article, text_blocks, image_blocks, audio_blocks, video_blocks)
        # return new article.id for redirect to edit page
        return HttpResponse(new_article.id)


# EDIT
class EditView(LoginRequiredMixin, View):
    def get(self, request, id):
        # render page article info for edit page
        article_blocks = (
            models.Block.objects
                .select_related("article", "block_text", "block_image", "block_audio", "block_video")
                .filter(article__pk=id)
                .order_by("position")
        )
        # if article have some blocks (text, video, etc) -> getting only article info
        # else if article have not any blocks (only image, title and annotation)
        if article_blocks:
            article = article_blocks[0].article
        else:
            article = models.Article.objects.get(pk=id)
            article_blocks = []
        date_list = str(article.on_change).split(" ")[0].split("-")
        date_list = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
        # return page, current article info, current article blocks, current article date, article list for contact form
        return render(request, "edit.html", {'article': article, 'article_blocks': article_blocks, 'article_date': date_list, 'articles_list': get_articles_title()})

    def post(self, request, id):
        # apply changes for current article
        with transaction.atomic():
            # getting files, data, order blocks list and list of deleted blocks
            files = request.FILES
            data = request.POST
            order = json.loads(data['order'])
            del_blocks = json.loads(data['del_blocks'])
            try:
                # try to save changes (image, title, annotation)
                this_article = models.Article.objects.get(id=id)
                this_article.title = data['inp_title_article']
                this_article.annotation = data['inp_title_annotation']
                # if have not image -> image did not change
                if 'inp_title_img' not in files:
                    pass
                else:
                    this_article.logo = files['inp_title_img']
                this_article.save()
            except IntegrityError as e:
                return HttpResponse(status=500, content="IntegrityError")
            text_blocks = []
            image_blocks = []
            audio_blocks = []
            video_blocks = []

            # if type of block is text ->
            #   change content and position of existed block
            #   if block not exist fill array for new blocks with name, obj.link and position block
            # if type of block isn't text (image, video, etc) ->
            #   change position of existed block
            #   if block not exist -> append it in array with name, obj.link and position
            for key in order:
                # if block not exist -> create block
                if order[key]['type'] == "html":
                    text_blocks.append({"name": key, "obj": models.BlockText(content=data[key]), 'pos': order[key]['pos']})
                # if block already exist -> update content current block by id and update his position
                if order[key]['type'] == "html_edit":
                    text_id = key.replace("text_", '')
                    models.BlockText.objects.filter(id=text_id).update(content=data[key])
                    models.Block.objects.filter(block_text__id=text_id).update(position=order[key]['pos'])
                elif order[key]['type'] == "image":
                    image_blocks.append({"name": key, "obj": models.BlockImage(content=files[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "image_edit":
                    models.Block.objects.filter(block_image__id=data[key]).update(position=order[key]['pos'])
                elif order[key]['type'] == "audio":
                    audio_blocks.append({"name": key, "obj": models.BlockAudio(content=files[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "audio_edit":
                    models.Block.objects.filter(block_audio__id=data[key]).update(position=order[key]['pos'])
                elif order[key]['type'] == "video":
                    video_blocks.append({"name": key, "obj": models.BlockVideo(content=files[key]), 'pos': order[key]['pos']})
                elif order[key]['type'] == "video_edit":
                    models.Block.objects.filter(block_video__id=data[key]).update(position=order[key]['pos'])

            # function to bulk_create all new blocks
            add_blocks(this_article, text_blocks, image_blocks, audio_blocks, video_blocks)

            # delete blocks
            for key in del_blocks:
                if del_blocks[key]['type'] == "html":
                    models.BlockText.objects.filter(id=del_blocks[key]['id']).delete()
                elif del_blocks[key]['type'] == "image":
                    models.BlockImage.objects.filter(id=del_blocks[key]['id']).delete()
                elif del_blocks[key]['type'] == "audio":
                    models.BlockAudio.objects.filter(id=del_blocks[key]['id']).delete()
                elif del_blocks[key]['type'] == "video":
                    models.BlockVideo.objects.filter(id=del_blocks[key]['id']).delete()

        return HttpResponse()


# ARTICLE
def article(request, token):
    try: # get article by id (for admins)
        token = int(token)
        if not request.user.is_authenticated:
            return redirect('login')
    except: # get article by token (from links)
        try:
            decoded_jwt = jwt.decode(token, AppConfig.JWT_KEY, algorithms=["HS256"])
            token = decoded_jwt['article_id']
        except jwt.ExpiredSignatureError:
            # error decode jwt
            return redirect('/')
        except:
            # other errors
            return redirect('/')
    article_blocks = (
        models.Block.objects
            .select_related("article", "block_text", "block_image", "block_audio", "block_video")
            .filter(article__pk=token)
            .order_by("position")
    )
    # if article have some blocks (text, video, etc) -> getting only article info
    # else if article have not any blocks (only image, title and annotation)
    if article_blocks:
        article = article_blocks[0].article
    else:
        article = models.Article.objects.get(pk=token)
        article_blocks = []
    # getting last 3 articles for advertising
    last_article = models.Article.objects.exclude(title=article.title).order_by('-on_change')[:3]
    # translate date on_change on russian
    for i in last_article:
        date_list = str(i.on_change).split(" ")[0].split("-")
        i.on_change = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
    date_list = str(article.on_change).split(" ")[0].split("-")
    date_list = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
    # return (page, current article info, current article blocks, last 3 articles,
    #         current article date, articles list for contact form)
    return render(request, "article.html", {'article': article, 'article_blocks': article_blocks, 'last_article': last_article, 'article_date': date_list, 'articles_list': get_articles_title()})


# ALL ARTICLES
def articles(request):
    # page for print all articles with lazy loading (not XD )
    if 'last_elem' not in request.GET: # loading page
        # if first time loading page get last 5 articles by on_change
        last_article = models.Article.objects.order_by('-on_change')[:5]
        # translate date on_change on russian
        for i in last_article:
            date_list = str(i.on_change).split(" ")[0].split("-")
            i.on_change = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
        # return page, last 5 articles, name of current page, and list of all articles for search script jquery
        return render(request, "articles.html", {'last_article': last_article, 'page': 'articles', 'articles_list': get_articles_title()})
    else:
        if 'type' not in request.GET: # get new articles after scroll page to bottom
            data = request.GET
            # get another 5 articles in range (all_befor_articles + 5 new articles)
            last_article = models.Article.objects.order_by('-on_change')[int(data['last_elem']):int(data['last_elem'])+5]
            articles = []
            # translate date on_change on russian and modify array for script jquery
            for i in last_article:
                date_list = str(i.on_change).split(" ")[0].split("-")
                i.on_change = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
                articles.append([i.logo.url, i.title, i.annotation, i.on_change, i.id])
            json_data = json.dumps({'articles': articles})
            # return json {'articles': [articles]}
            return HttpResponse(json_data)
        elif 'type' in request.GET: # if search (select) only one article -> getting her
            data = request.GET
            article = models.Article.objects.get(pk=int(data['last_elem']))
            articles = []
            date_list = str(article.on_change).split(" ")[0].split("-")
            article.on_change = date_list[2] + " " + month_list[int(date_list[1]) - 1] + ', ' + date_list[0] + ' г.'
            articles.append([article.logo.url, article.title, article.annotation, article.on_change, article.id])
            # return json {'articles': [article]}
            json_data = json.dumps({'articles': articles})
            return HttpResponse(json_data)


# DELETE
class DeleteView(LoginRequiredMixin, View):
    def post(self, request, id):
        # delete article by id
        data = request.POST
        models.Article.objects.filter(id=data['article_id']).delete()
        return HttpResponse()


# CREATE TOKEN
class CreateTokenView(LoginRequiredMixin, View):
    def get(self, request):
        # generate JWT Token for selected article with binded key (jwt_k)
        # return json "{token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9..."}"
        # (example)
        data = request.GET
        encoded_jwt = jwt.encode(
            {"article_id": data['article_id'],
             "exp": datetime.now(tz=timezone.utc) + timedelta(days=14)},
            AppConfig.JWT_KEY,
            algorithm="HS256"
        )
        json_data = json.dumps({'token': encoded_jwt})
        return HttpResponse(json_data)

