from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('create/', views.CreateView.as_view(), name='create'),
    path('article/<str:token>', views.article, name='article'),
    path('articles/', views.articles, name='articles'),
    path('edit/<int:id>', views.EditView.as_view(), name='edit'),
    path('edit/<int:id>/delete/', views.DeleteView.as_view(), name='delete'),
    path('ct/', views.CreateTokenView.as_view(), name='create_token'),
    path('accounts/', include('django.contrib.auth.urls')),
]
