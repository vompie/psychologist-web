from django.contrib import admin

from . import models

# Register your models here.


@admin.register(models.Article)
class ArticleAdmin(admin.ModelAdmin):
    model = models.Article
    list_display = ['id', 'title', 'annotation', 'on_create', 'on_change']
    list_filter = ['title', 'on_create', 'on_change']
    search_fields = ['id', 'title', 'annotation']


@admin.register(models.Block)
class BlockAdmin(admin.ModelAdmin):
    model = models.Block
    list_display = ['id', 'article', 'type', 'position', 'block_text', 'block_image', 'block_audio', 'block_video']
    # 'on_create', 'on_change']
    list_filter = ['type', 'on_create', 'on_change']
    search_fields = ['id', 'article']


