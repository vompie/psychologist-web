import sys
from django.apps import AppConfig
from .telegram import start_thread_tlg
from dotenv import load_dotenv
import os


load_dotenv()


class AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'
    tlg = None
    JWT_KEY = os.getenv("JWT_KEY")
    API_TOKEN = os.getenv("TLG_API_TOKEN")
    CHAT_ID = int(os.getenv("TLG_CHAT_ID"))
    DOMAIN = os.getenv("DOMAIN")
    PROTOCOL = os.getenv("PROTOCOL")

    def ready(self):
        # return
        # start telegram thread and get obj of telegram bot
        if not sys.argv[1] in [
            "makemigrations", 
            "migrate", 
            'collectstatic', 
            'changepassword', 
            'createsuperuser', 
            'optimizemigration', 
            'collectstatic', 
            'findstatic',
            'clearsessions',
            'showmigrations',
            'loaddata',
            'inspectdb',
            'flush',
            'dumpdata',
            'dbshell'
        ]: # Prevent execute in some manage command
            AppConfig.tlg = start_thread_tlg(AppConfig.API_TOKEN,
                                                AppConfig.CHAT_ID,
                                                AppConfig.JWT_KEY,
                                                AppConfig.DOMAIN,
                                                AppConfig.PROTOCOL)
