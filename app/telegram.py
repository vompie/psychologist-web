import telebot
import threading
import traceback
import time
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton
from datetime import datetime, timedelta, timezone
import jwt


class TelegramBot:
    def __init__(self, API_TOKEN, CHAT_ID, JWT_KEY, DOMAIN, PROTOCOL):
        from . import models
        self.models = models
        self.API_TOKEN = API_TOKEN
        self.CHAT_ID = CHAT_ID
        self.JWT_KEY = JWT_KEY
        self.DOMAIN = DOMAIN
        self.PROTOCOL = PROTOCOL
        self.bot = telebot.TeleBot(self.API_TOKEN)
        self.count_on_page = 5

        # All message
        @self.bot.channel_post_handler()
        def send_welcome(message):
            # check chat id
            try:
                if message.chat.id != self.CHAT_ID:
                    return
                markup = InlineKeyboardMarkup()
                markup.add(InlineKeyboardButton(text='👉 тык 👈', callback_data='alist'))
                self.bot.send_message(self.CHAT_ID, text="Нажмите для создания ссылки на лекцию 📝", reply_markup=markup)
            except:
                pass

        # CallBack
        @self.bot.callback_query_handler(func=lambda call: True)
        def callback_query(call):
            try:
                req = call.data.split('_')
                if req[0] == 'cl': # create link to article
                    data = self.create_token(int(req[1]))
                    url = "{0}://{1}/article/{2}".format(self.PROTOCOL, self.DOMAIN, data[0])
                    txt = "{0}\n\nДата создания: {1}\nДата окончания: {2}\n\n{3}".format(req[2], data[1], data[2], url)
                    markup = InlineKeyboardMarkup()
                    markup.add(InlineKeyboardButton(text=req[2], url=url))
                    self.bot.send_message(self.CHAT_ID, text=txt, reply_markup=markup)
                if req[0] == 'alist':   # list of articles
                    count = models.Article.objects.count()
                    articles_list = models.Article.objects.values('id', 'title').order_by('-on_create')[:self.count_on_page]
                    markup = InlineKeyboardMarkup()
                    for article in articles_list:   # add articles in massage
                        markup.add(InlineKeyboardButton(text=article['title'], callback_data='cl_{0}_{1}'.format(article['id'], article['title'])))
                    if len(articles_list) < count:  # if need one more page
                        markup.add(
                            InlineKeyboardButton(text="🚫      ", callback_data='empty'),
                            InlineKeyboardButton(text="Страница №1", callback_data='empty'),
                            InlineKeyboardButton(text="Вперед 👉", callback_data='next_{0}_{1}'.format(self.count_on_page, 1))
                        )
                    self.bot.send_message(self.CHAT_ID, text="📝 Выберите лекцию: ", reply_markup=markup)
                if req[0] == 'next':
                    count = models.Article.objects.count()
                    pos = int(req[1])
                    page = int(req[2])
                    articles_list = models.Article.objects.values('id', 'title').order_by('-on_create')[pos:pos+self.count_on_page]
                    markup = InlineKeyboardMarkup()
                    for article in articles_list:  # add articles in massage
                        markup.add(InlineKeyboardButton(text=article['title'],
                                                        callback_data='cl_{0}_{1}'.format(article['id'], article['title'])))
                    if len(articles_list) + pos < count:    # if need next pages
                        markup.add(
                            InlineKeyboardButton(text="👈 Назад", callback_data='back_{0}_{1}'.format(pos, page+1)),
                            InlineKeyboardButton(text="Страница №{}".format(page+1), callback_data='empty'),
                            InlineKeyboardButton(text="Вперед 👉", callback_data='next_{0}_{1}'.format(pos + self.count_on_page, page+1))
                        )
                    else:
                        markup.add(
                            InlineKeyboardButton(text="👈 Назад", callback_data='back_{0}_{1}'.format(pos, page+1)),
                            InlineKeyboardButton(text="Страница №{}".format(page + 1), callback_data='empty'),
                            InlineKeyboardButton(text="       🚫", callback_data='empty')
                            )
                    self.bot.edit_message_text(text="📝 Выберите лекцию: ", reply_markup=markup, chat_id=self.CHAT_ID,
                                          message_id=call.message.message_id)
                if req[0] == 'back':
                    pos = int(req[1])
                    page = int(req[2])
                    prev = pos - self.count_on_page if pos - self.count_on_page >= 0 else 0
                    articles_list = models.Article.objects.values('id', 'title').order_by('-on_create')[prev:pos]
                    markup = InlineKeyboardMarkup()
                    for article in articles_list:  # add articles in massage
                        markup.add(InlineKeyboardButton(text=article['title'],
                                                        callback_data='cl_{0}_{1}'.format(article['id'], article['title'])))
                    if prev != 0:   # if need prev page
                        markup.add(
                            InlineKeyboardButton(text="👈 Назад", callback_data='back_{0}_{1}'.format(prev, page-1)),
                            InlineKeyboardButton(text="Страница №{}".format(page-1), callback_data='empty'),
                            InlineKeyboardButton(text="Вперед 👉", callback_data='next_{0}_{1}'.format(pos, page-1))
                        )
                    else:
                        markup.add(
                            InlineKeyboardButton(text="🚫      ", callback_data='empty'),
                            InlineKeyboardButton(text="Страница №{}".format(page - 1), callback_data='empty'),
                            InlineKeyboardButton(text="Вперед 👉", callback_data='next_{0}_{1}'.format(pos, page - 1))
                        )
                    self.bot.edit_message_text(text="📝 Выберите лекцию: ", reply_markup=markup, chat_id=self.CHAT_ID,
                                          message_id=call.message.message_id)
            except:
                self.bot.send_message(self.CHAT_ID, text="Случилась непредвиденная ошибка 😞")

    # Send form from django
    def send_form(self, msg, article_id="", article_title="", session=False):
        try:
            if not session:
                markup = InlineKeyboardMarkup()
                markup.add(InlineKeyboardButton(text='Создать ссылку 🔗', callback_data='cl_{0}_{1}'.format(article_id, article_title)))
                self.bot.send_message(self.CHAT_ID, text=msg, reply_markup=markup)
            else:
                self.bot.send_message(self.CHAT_ID, text=msg)
        except:
            pass

    # Create token
    def create_token(self, id):
        try:
            date_now = datetime.now(tz=timezone.utc)
            date_dur = (date_now + timedelta(days=14)).strftime("%d.%m.%Y %H:%M:%S")
            encoded_jwt = jwt.encode(
                {"article_id": id,
                 "exp": date_now + timedelta(days=14)},
                self.JWT_KEY,
                algorithm="HS256"
            )
            return [encoded_jwt, date_now.strftime("%d.%m.%Y %H:%M:%S"), date_dur]
        except:
            return ["X", "Ошибки генерациии ссылки", "Попробуйте позже"]

    # Start telegram
    def start_telegram(self):
        while True:
            try:
                self.bot.polling(none_stop=True, interval=0)
            except:
                pass
                # print(traceback.format_exc())
            time.sleep(1)


# Start thread
def start_thread_tlg(API_TOKEN, CHAT_ID, JWT_KEY, DOMAIN, PROTOCOL):
    tlg = TelegramBot(API_TOKEN, CHAT_ID, JWT_KEY, DOMAIN, PROTOCOL)
    t = threading.Thread(target=tlg.start_telegram)
    t.start()
    tlg.bot.send_message(CHAT_ID, text="Бот запущен и готов к работе 👌")
    return tlg


